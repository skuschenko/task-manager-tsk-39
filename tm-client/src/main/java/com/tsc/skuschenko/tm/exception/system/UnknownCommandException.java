package com.tsc.skuschenko.tm.exception.system;

import com.tsc.skuschenko.tm.exception.AbstractException;
import org.jetbrains.annotations.Nullable;

public final class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Command does not found...");
    }

    public UnknownCommandException(@Nullable final String command) {
        super("Error! Command '" + command + "' does not found...");
        System.out.println("Pleas print 'help' for more information");
    }

}
