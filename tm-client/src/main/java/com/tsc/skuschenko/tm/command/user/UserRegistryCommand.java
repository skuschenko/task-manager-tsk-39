package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.User;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "registry user";

    @NotNull
    private static final String NAME = "registry-user";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo("password");
        @NotNull final String password = TerminalUtil.nextLine();
        showParameterInfo("email");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final User user = serviceLocator.getUserEndpoint()
                .createUserWithEmail(login, password, email);
        showUser(user);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
